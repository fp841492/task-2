#!/usr/bin/env python
# coding: utf-8

# In[1]:
#improt 

import threading
from collections import defaultdict, Counter
from queue import Queue
from concurrent.futures import ThreadPoolExecutor

# Function to read data from a file line by line
def read_data_from_file(file_path):
    with open(file_path, 'r') as file:
        return file.readlines()

# Function for the map phase: Extract passenger IDs from the flight data
def map_phase(input_data, output_queue):
    for record in input_data:
        record = record.strip()
# Check if the record is not empty
        if record: 
            passenger_id = record.split(',')[0]  # Extract the passenger ID
            output_queue.put(passenger_id)  # Put the passenger ID into the output queue

# Function for the shuffle phase: Group by passenger ID
def shuffle_phase(output_queue):
    results = defaultdict(list)
    while not output_queue.empty():
        passenger_id = output_queue.get()
        results[passenger_id].append(1)  # Append a placeholder to count flights later
    return results

# Function for the reduce phase: Count the flights per passenger
def reduce_phase(shuffled_data):
    # Sum the list of ones for each passenger to get the flight count
    reduced_data = {passenger: sum(flights) for passenger, flights in shuffled_data.items()}
    return reduced_data

# Main function to process passenger data
def main(passenger_data_file, airport_data_file):
    # Read data from files
    passenger_data = read_data_from_file(passenger_data_file)
    # Note: airport_data is read but not used directly in this example
    airport_data = read_data_from_file(airport_data_file)
    
    output_queue = Queue()  # Queue to hold output from map phase
    
    # Process Passenger Data using a ThreadPoolExecutor to manage threads
    with ThreadPoolExecutor(max_workers=10) as executor:
        # Divide the data into chunks and submit each chunk to the map_phase function
        for chunk in chunks(passenger_data, 100):
            executor.submit(map_phase, chunk, output_queue)
    
    # Shuffle phase: Group data by passenger ID
    shuffled_data = shuffle_phase(output_queue)
    # Reduce phase: Count the flights for each passenger
    reduced_data = reduce_phase(shuffled_data)
    
    # Find and return the top passengers with the highest flight count
    top_passengers, flights = find_top_passengers(reduced_data)
    return top_passengers, flights

# Utility function to divide the list into chunks of size n
def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

# Function to find the top passengers based on flight count
def find_top_passengers(reduced_data):
    # Use Counter to easily find the highest flight count
    counter = Counter(reduced_data)
    highest_flight_count = counter.most_common(1)[0][1]  # Get the highest flight count
    # Find all passengers with the highest flight count
    top_passengers = [passenger for passenger, flights in counter.items() if flights == highest_flight_count]
    return top_passengers, highest_flight_count

if __name__ == "__main__":
    # Execute the main function and print the results
    top_passengers, flights = main("AComp_Passenger_data.csv", "Top30_airports_LatLong.csv")
    print(f"Top Passenger(s): {top_passengers} with {flights} flights")

